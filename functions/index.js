const mailchimp = require('./utils/mailchimp');
const mailgun = require('./utils/mailgun');

// The Cloud Functions for Firebase SDK to create Cloud Functions and setup triggers.
const functions = require('firebase-functions');

// The Firebase Admin SDK to access the Firebase Realtime Database.
const admin = require('firebase-admin');
admin.initializeApp(functions.config().firebase);

// // Create and Deploy Your First Cloud Functions
// // https://firebase.google.com/docs/functions/write-firebase-functions
//
// exports.helloWorld = functions.https.onRequest((request, response) => {
//  response.send("Hello from Firebase!");
// });


// // Take the text parameter passed to this HTTP endpoint and insert it into the
// // Realtime Database under the path /messages/:pushId/original
// exports.addMessage = functions.https.onRequest((req, res) => {
//   // Grab the text parameter.
//   const original = req.query.text;
//   // Push the new message into the Realtime Database using the Firebase Admin SDK.
//   admin.database().ref('/messages').push({original: original}).then(snapshot => {
//     // Redirect with 303 SEE OTHER to the URL of the pushed object in the Firebase console.
//     res.redirect(303, snapshot.ref);
//   });
// });

exports.newRsvp = functions.database.ref('/rsvp/{pushId}')
  .onWrite(event => {
    const rsvp = event.data.val();

    mailchimp.subscribe(rsvp)
      // don't allow fail on mailchimp alone
      .catch((e) => {
        console.log(e);
      });

    mailgun.send('notification', rsvp)
      // don't allow fail on email alone
      .catch((e) => {
        console.log(e);
      });
  });

exports.newSong = functions.database.ref('/playlist/{pushId}')
  .onWrite(event => {
    const playlist = event.data.val();

    mailgun.send('playlist', playlist)
      // don't allow fail on email alone
      .catch((e) => {
        console.log(e);
      });
  });
