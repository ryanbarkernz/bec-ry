/*
  Util: Mailchimp
   - subscribe citizens to Mailchimp
*/

// mailchimp V3 api
const Mailchimp = require('mailchimp-api-v3');
const mailchimp_api_key = "039fd11c24463c74987b94d2824946a7-us6";
const mailchimp_list_id = "380bbbe8a0";

exports.subscribe = (data) => {
  var mailchimp = new Mailchimp(mailchimp_api_key);

  let list = data.list || mailchimp_list_id,
  hash = require('crypto').createHash('md5').update(data.email).digest("hex"),
  subscribeData = {
    email_address: data.email,
    status: "subscribed",
    merge_fields: {
        NAME: data.name,
        EMAIL: data.email,
        DIET: data.diet,
        ATTENDING: data.attending
    }
  };

  if(data.ip) {
    Object.assign(subscribeData, {
      ip_signup: data.ip,
      ip_opt: data.ip,
    });
  }

  return mailchimp.put(`lists/${list}/members/${hash}`, subscribeData);
}