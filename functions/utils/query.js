/*
  Util: Query
   - check and format parsed fields
   - respond with success/error
*/

// Firebase
const admin = require('./firebase-admin');

// CORS
const cors = require('cors')({origin: true});

// Utils
const codec = require('./codec');
const shortcode = require('./../utils/shortcode');

const capitalise = (str, restLower) => {
  return str.charAt(0).toUpperCase() + (restLower===true ? str.slice(1).toLowerCase() : str.slice(1));
}

module.exports = function (req, res) {

  var errorMSG = '';

  const parse = {
    name: (name) => {
      let split = name.split(/\s+/g)
      assumeBusiness = split.length >= 4,
      namedata = {
        raw: name,
        first: assumeBusiness ? "" : capitalise(split[0], true),
        last: split.slice(assumeBusiness ? 0 : 1).map(capitalise).join(" "),
      };

      return namedata;
    },

    referrer: (referrer) => {

      // decode into array
      const decoded = [].concat(referrer).map((referrer) => {
        return Object.assign({
          token: referrer
        }, codec.decode(referrer));
      });

      // return single or array
      return typeof referrer === "string" ? decoded[0] : decoded;
    },

    email: (email) => {
      return email.toLowerCase();
    },

    coords: (coords) => {
      return coords.split(",").reduce((coords, value, i) => {
        coords[i ? 'lng' : 'lat'] = +value;
        return coords;
      }, {});
    },
  };

  this.data = {
    time: Date.now(), 
    ip: req.headers['x-forwarded-for'] || req.connection.remoteAddress,
  };

  this.has = (fields) => {
    // a:b allows fields to be aliased
    // e.g. to be sent as 'b' but recorded as 'a'
    return fields.every((field) => {
      let split = field.split(":");
      let key = split.slice(-1)[0];
      return req.query.hasOwnProperty(key);
    });
  };

  this.shortcode = (referral, data, type) => {
    type = data ? type || 'action' : 'citizen';
    return shortcode.create(type, this.data.uid, data ? Object.assign({
      [type]: data,
    }, referral) : referral);
  };
 
  this.error = (e) => {

    // if message is predefined consider this a controlled exit
    let controlled = e.message === errorMSG;

    if(!controlled) console.log(e);

    cors(req, res, () => {

      // if controlled send an OK status, otherwise send an error
      res.status(controlled ? 200 : 500).json({
        success: false,

        // only display unique message if controlled
        // msg: controlled ? e.message : e.message//'there was an error',
        message: e.message,
      });
    });
  };

  this.respond = {
    success: (args, status) => {
      cors(req, res, () => {
        res.status(status || 200).json(Object.assign({
          success: true,
        }, args));
      });
    },

    error: (error, throwError) => {
      errorMSG = error;
      if(throwError) throw new Error(error);
      else this.error({
        message: error
      });
    },
  };

  // returns a promise
  this.parse = (fieldsRequired, fieldsOptional) => {

    // create a new promise to verify the token
    return new Promise((resolve, reject) => {
      if(!req.query.token) resolve();
      else admin.auth().verifyIdToken(req.query.token)
      .then((decoded) => {
        resolve(decoded.uid);
      }).catch((error) => {
        resolve();
      });
    })
    .then((uid) => {
      if(uid) this.data.uid = uid;
      else if(req.query.token) this.respond.error('invalid token', true);

      // check for missing fields
      if(fieldsRequired && !this.has(fieldsRequired)) this.respond.error('missing fields', true);

      // parse fields
      return Promise.all((fieldsRequired || []).concat(fieldsOptional || []).map((field) => {

        // a:b allows fields to be aliased
        // e.g. to be sent as 'b' but recorded as 'a'
        const split = field.split(":");
        const key = split.slice(-1)[0];
        field = split[0]; // decalred as first argument
        const value = !req.query.hasOwnProperty(key) ? "" :
          parse.hasOwnProperty(field) ? parse[field].bind(this)(req.query[key]) :
          req.query[key];

        return Promise.resolve(value)
        .then(value => {
          this.data[field] = value;
        });
      }));
    })
    .then(() => {
      return this.data;
    });
  };
}
