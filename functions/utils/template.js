/*
  Util: Template
   - simple templating engine
   e.g. template("a <%= type %> template")({type: cool})
*/

exports.file = (file) => {
    const str = require('fs').readFileSync(file, "utf8");
    return exports.string(str);
};


exports.string = (str) => {
    // Adapted from http://goo.gl/7KvWuU with single-quote fix from http://goo.gl/fOuW28
    // John Resig - http://ejohn.org/ - MIT Licensed
    // Figure out if we're getting a template, or if we need to
    // load the template - and be sure to cache the result.
    var fn = !/\W/.test(str) ? template[str] = template[str] || this.template($(str)[0].innerHTML) : // Generate a reusable function that will serve as a template
    // generator (and which will be cached).
    new Function('obj', 'var p=[],print=function(){p.push.apply(p,arguments);};' + // Introduce the data as local variables using with(){}
    'with(obj){p.push(\'' + // Convert the template into pure JavaScript
    str.replace(/[\r\t\n]/g, ' ').replace(/'(?=[^%]*%>)/g, '\t').split('\'').join('\\\'').split('\t').join('\'').replace(/<%=(.+?)%>/g, '\',$1,\'').replace(/{{(.+?)}}/g, '\',$1,\'').split('<%').join('\');').split('%>').join('p.push(\'') + '\');}return p.join(\'\');');
    // Provide some basic currying to the user
    return function (data) {
      return fn(data);
    };
};