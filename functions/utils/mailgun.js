/*
  Util: Mailgun
   - send emails
   - https://gist.github.com/vlucas/2bd40f62d20c1d49237a109d491974eb
*/

const path = require('path');

// Mailgun
const api_key = 'key-ddc1dd0ee5cfb5add71d56a8aa83d057';
const domain = 'hello.becandry.com';
const mailgun = require('mailgun-js')({apiKey: api_key, domain: domain});

// Utils
const Template = require("./template");

// 
const templates = {
  notification: {
    subject: "<%= name %> just RSVP'd.",
    html: "./../templates/email/notification.html",
    text: "./../templates/email/notification.txt",
  },
  playlist: {
    subject: "<%= title %> by <%= artist %> added.",
    html: "./../templates/email/playlist.html",
    text: "./../templates/email/playlist.txt",
  }
};

exports.send = (type, data) => {
  const template = templates[type];
  let emails = 'ryanbarker.nz@gmail.com, rebecca.stonehouse@outlook.com';

  data.tag = data.tag || type;
  if (data.tag == 'playlist') emails = 'ryanbarker.nz@gmail.com';

  return new Promise((resolve, reject) => {

    mailgun.messages().send({
      from: 'Bec & Ry <us@becandry.com>',
      to: emails,
      // to: data.email,
      subject: Template.string(template.subject)(data),
      html: Template.file(path.resolve(__dirname, template.html))(data),
      text: Template.file(path.resolve(__dirname, template.text))(data),
      'o:tag': [data.tag]
    }, function (error, body) {
      if(error) reject(error);
      else resolve(body);
    });
  });
}