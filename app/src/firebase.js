// Initialize firebase
import firebase from 'firebase';

const config = {
  apiKey: 'AIzaSyAbxZ3o4KKO8JupGrtohBsIRRppAHVayoU',
  authDomain: 'bec-and-ry.firebaseapp.com',
  databaseURL: 'https://bec-and-ry.firebaseio.com',
  projectId: 'bec-and-ry',
  storageBucket: 'bec-and-ry.appspot.com',
  messagingSenderId: '950397724669',
};
const firebaseApp = firebase.initializeApp(config);
const db = firebaseApp.database();

export default db;
