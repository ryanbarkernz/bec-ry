/*eslint-disable */
if (!String.prototype.includes) {
  String.prototype.includes = function() {
      return String.prototype.indexOf.apply(this, arguments) !== -1;
  };
}

if (typeof NodeList.prototype.forEach !== "function") {
  NodeList.prototype.forEach = Array.prototype.forEach;
}
/*eslint-enable */
