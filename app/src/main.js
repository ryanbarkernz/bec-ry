// import firebase from 'firebase';

// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import 'what-input';
import 'whatwg-fetch';

import Vue from 'vue';
import VueFire from 'vuefire';
import Meta from 'vue-meta';
import WebFont from 'webfontloader';
import svg4everybody from 'svg4everybody';
import objectFitImages from 'object-fit-images';
import objectFitVideos from 'object-fit-videos';
import { VueMasonryPlugin } from 'vue-masonry';

import './custom-polyfills';
import App from './App';
import router from './router';
import store from './store';

Vue.use(Meta);
Vue.use(VueFire);
Vue.use(VueMasonryPlugin);

Vue.config.productionTip = false;

/* eslint-disable no-new */
new Vue({
  el: '#app',
  router,
  store,
  template: '<App/>',
  components: { App },
  mounted() {
    svg4everybody();
    objectFitImages();
    objectFitVideos();
    WebFont.load({
      google: {
        families: ['Montserrat:300,400'],
      },
      custom: {
        families: ['Jasmine'],
      },
    });

    this.$router.beforeEach((to, from, next) => {
      this.$store.commit('setNavState', false);
      window.scrollTo(0, 0);
      next();
    });
  },
});
