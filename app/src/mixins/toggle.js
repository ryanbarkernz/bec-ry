import store from '../store';

export default {
  name: 'toggle',
  methods: {
    toggleAuthPopup() {
      const currentAuthState = store.state.auth;
      currentAuthState.isOpen = !currentAuthState.isOpen;
      store.commit('setAuth', currentAuthState);
    },

    toggleNav() {
      const currentNavState = store.state.isNavOpen;
      store.commit('setNavState', !currentNavState);
    },

    toggleUser() {
      const currentUserState = store.state.isUserOpen;
      store.commit('setUserState', !currentUserState);
      store.commit('setUserStats', { newNotifications: 0 });
    },
  },
};
