
import Icon from '@/components/Icon';
import { openPopupWindow } from '../utils';
import store from '../store';

export default {
  name: 'share-buttons',
  data() {
    return {
      copyLinkEnabled: false,
      facebook: '#',
      twitter: '#',
    };
  },
  components: {
    icon: Icon,
  },
  computed: {
    shortCode() {
      return store.getters.getUserShortCode;
    },
    fullUrl() {
      return `${window.location.origin}/s/${this.shortCode}`;
    },
  },
  methods: {
    shareLink(type) {
      /* eslint-disable */
      // console.log('log');
      // const siteTitle = encodeURIComponent(document.title);
      // const currentUrl = encodeURIComponent(window.location.origin);

      const siteTitle = 'I’m a citizen of the Great Barrier Reef – join me.';
      const currentUrl = encodeURIComponent(window.location.origin);

      // @TODO this needs to be the shortened version
      const refUrl = currentUrl + '/s/' + encodeURIComponent(`${this.shortCode}`);

      // @TODO get final copy for twitter
      const twitterBody = 'I\'m now a citizen of the Great Barrier Reef, committed to the reef\'s future. Join me';
      const emailBody = twitterBody + ' ' + refUrl;
      let url;

      if (type === 'fb') {
        url = `https://www.facebook.com/sharer.php?u=${refUrl}`;
        openPopupWindow(url);
      } else if (type === 'twitter') {
        url = `https://twitter.com/intent/tweet?text=${twitterBody}&url=${refUrl}&hashtags=citizensGBR&via=citizensGBR`;
        openPopupWindow(url);
      } else if (type === 'email') {
        url = `mailto:?subject=${siteTitle}&body=${emailBody}`;
        location.href = url;
      }
    },

    getLinkText(e) {
      const copyInput = e.target.classList;
      if (!copyInput.contains('c-share-input')) {
        this.toggleClass(e.target, 'c-share-input-wrapper');
        this.copyLinkEnabled = !this.copyLinkEnabled;
      }
    },

    selectUrl(e) {
      e.target.setSelectionRange(0, e.target.value.length)
    },

    toggleClass(el, classToToggle) {
      if (el.classList.contains(classToToggle)) {
        el.classList.remove(classToToggle);
      } else {
        el.classList.add(classToToggle);
      }
    }
  },
};
