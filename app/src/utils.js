/* eslint-disable */
export function getParameterByName(name) {
  const url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  const regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
      results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

export function log(msg, error) {
  // @todo: store logs on firebase!?
  // console.warn(msg, error);
}

export function openPopupWindow(url) {
  const width = 575;
  const height = 255;
  const dualScreenLeft = window.screenLeft !== undefined ? window.screenLeft : window.screen.left;
  const dualScreenTop = window.screenTop !== undefined ? window.screenTop : window.screen.top;
  const options = [
    'status=1',
    `width=${width}`,
    `height=${height}`,
    `left=${Math.ceil(((window.innerWidth / 2) - (width / 2)) + dualScreenLeft)}`,
    `top=${Math.ceil(((window.innerHeight / 2) - (height / 2)) + dualScreenTop)}`,
  ].join(',');
  window.open(url, '_blank', options);
}

export function createDataUri(data) {
  return Object.keys(data).map((i) => i+'='+encodeURIComponent(data[i])).join('&');
}

export function clearUrl() {
  if ('pushState' in window.history) {
    window.history.pushState({}, document.title, '/');
  }
}

export function copyToClipboard($el) {
  // https://developers.google.com/web/updates/2015/04/cut-and-copy-commands
  const range = document.createRange();
  range.selectNode($el);  
  window.getSelection().removeAllRanges();
  window.getSelection().addRange(range);
  let copied = false;

  try {
    copied = document.execCommand('copy');
    $el.dataset.copied = 1;
    setTimeout(() => $el.dataset.copied = 0, 2000);
  } catch(err) {}

  // Remove the selections - NOTE: Should use
  // removeRange(range) when it is supported  
  window.getSelection().removeAllRanges();

  return copied;
}
