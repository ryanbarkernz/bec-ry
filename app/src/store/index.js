import Vue from 'vue';
import Vuex from 'vuex';
// import createPersistedState from 'vuex-persistedstate';

Vue.use(Vuex);

export default new Vuex.Store({
  // plugins: [createPersistedState()],
  state: {
    isNavOpen: false,
    isLoading: false,
    isSticky: false,
  },
  mutations: {
    setNavState(state, navState) {
      state.isNavOpen = navState;
    },
    setLoading(state, loading) {
      state.isLoading = loading;
    },
    setScrollPosition(state, flag) {
      state.isSticky = flag;
    },
  },
});
