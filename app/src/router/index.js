import Vue from 'vue';
import Router from 'vue-router';
import Home from '@/pages/Home';
import Engagement from '@/pages/Engagement';
// import Rsvp from '@/pages/Rsvp';
import Venue from '@/pages/Venue';
import Entertainment from '@/pages/Entertainment';
import WhatsHappening from '@/pages/WhatsHappening';

Vue.use(Router);

export default new Router({
  mode: 'history',
  linkActiveClass: 'is-active',
  routes: [
    {
      path: '/',
      name: 'home',
      component: Home,
    },
    {
      path: '/whats-happening',
      name: 'whats-happening',
      component: WhatsHappening,
    },
    {
      path: '/venue',
      name: 'venue',
      component: Venue,
    },
    {
      path: '/entertainment',
      name: 'entertainment',
      component: Entertainment,
    },
    {
      path: '/engagement',
      name: 'engagement',
      component: Engagement,
    },
  ],
});
